import React from 'react';
import { configure, storiesOf } from '@storybook/react';
import { Welcome } from '@storybook/react/demo';
import { linkTo } from '@storybook/addon-links';

import '@fortawesome/fontawesome-pro/scss/fontawesome.scss';
import '@fortawesome/fontawesome-pro/scss/regular.scss';
import '@fortawesome/fontawesome-pro/scss/solid.scss';
import '@fortawesome/fontawesome-pro/scss/brands.scss';

import './index.scss';

storiesOf('Welcome', module)
  .add('to Storybook', () => <Welcome showApp={linkTo('Button')} />);
;

// automatically import all files ending in *.stories.js
const req = require.context('../src', true, /\.story.js?$/);
function loadStories() {
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);
