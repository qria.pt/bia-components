import React from 'react';
import PropTypes from 'prop-types';
import raf from 'raf';
import ClassNames from 'classnames';

export default class ScrollInStyle extends React.Component {
  static propTypes = {
    disabled: PropTypes.bool,
    scrollInHeight: PropTypes.number,
    className: PropTypes.string,
    onScrollStyle: PropTypes.object,
    onScrollClassName: PropTypes.string,
  };

  static defaultProps = {
    disabled: false,
    scrollInHeight: 100,
    className: '',
    onScrollStyle: {},
    onScrollClassName: '',
  };

  constructor(props) {
    super(props);

    this.state = {
      scrolled: false,
    };

    this.handlingScrollUpdate = false;
  }

  getScrollY = () => {
    if (window.pageYOffset !== undefined) {
      return window.pageYOffset;
    } else if (window.scrollTop !== undefined) {
      return window.scrollTop;
    } else {
      return (document.documentElement || document.body.parentNode || document.body).scrollTop;
    }
  };

  handleScroll = () => {
    if (!this.handlingScrollUpdate) {
      this.handlingScrollUpdate = true;
      raf(this.update);
    }
  };

  update = () => {
    const currentScrollY = this.getScrollY();

    this.setState({
      scrolled: currentScrollY >= this.props.scrollInHeight,
    });

    this.handlingScrollUpdate = false;
  };

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  render() {
    const {
      disabled,
      className,
      onScrollStyle,
      onScrollClassName,
      children,
    } = this.props;
    const {scrolled} = this.state;
    return (
      <div
        style={!disabled && scrolled ? onScrollStyle : null}
        className={ClassNames(
          className,
          {[`${onScrollClassName}`]: !disabled && scrolled},
        )}
      >
        {children}
      </div>
    );
  }
}
