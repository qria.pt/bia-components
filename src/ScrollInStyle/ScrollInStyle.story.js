import React from 'react';
import { storiesOf } from '@storybook/react';

import ScrollInStyle from './index';
import './ScrollInStyle.story.scss';

storiesOf('ScrollInStyle', module)
  .add('with onScrollStyle', () => (
    <div style={{ height: '200vh' }}>
      <div style={{ height: '350px'} }/>
      <ScrollInStyle
        onScrollStyle={{ backgroundColor: '#df2955 '}}
      >
        Scroll me up for background color with style
      </ScrollInStyle>
    </div>
  ))

  .add('with onScrollClassName', () => (
    <div style={{ height: '200vh' }}>
      <div style={{ height: '350px'} }/>
      <ScrollInStyle
        onScrollClassName="gradient"
      >
        Scroll me up for background image with class
      </ScrollInStyle>
    </div>
  ))

  .add('with default className', () => (
    <div style={{ height: '200vh' }}>
      <div style={{ height: '350px'} }/>
      <ScrollInStyle
        className="gradient"
        onScrollClassName="reverse"
      >
        Scroll me up for to change the default class background
      </ScrollInStyle>
    </div>
  ))

  .add('with disabled', () => (
    <div style={{ height: '200vh' }}>
      <div style={{ height: '350px'} }/>
      <ScrollInStyle
        className="gradient"
        onScrollClassName="reverse"
        disabled
      >
        Scroll me up, but nothing will happen
      </ScrollInStyle>
    </div>
  ))

  .add('with scrollInHeight', () => (
    <div style={{ height: '200vh' }}>
      <div style={{ height: '350px'} }/>
      <ScrollInStyle
        onScrollClassName="gradient"
        scrollInHeight={330}
      >
        Scroll me to the top
      </ScrollInStyle>
    </div>
  ))
;
