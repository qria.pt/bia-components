import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ClassNames from 'classnames';

import {isLightColored} from '../index';
import './Ranking.scss';

class Ranking extends PureComponent {
  static propTypes = {
    ranking: PropTypes.number,
    small: PropTypes.bool,
    colorTheme: PropTypes.string,
  };

  renderRanking() {
    const { ranking, small, colorTheme} = this.props;
    const stars = [];
    const starCount = Math.floor(ranking);

    for(let i=0; i<starCount; i++){
      stars.push(<span key={i} className="fas fa-star" />);
    }

    for (let i = starCount; i < 5; i++) {
      if(ranking - i > 0){
        stars.push(<span key={i} className="fas fa-star-half-alt" />);
      } else {
        stars.push(<span key={i} className="far fa-star" />);
      }
    }

    return (
      <div
        className={ClassNames(
          'ranking-stars',
          {'ranking-stars--small': small},
        )}
        style={{color: isLightColored(colorTheme) ? '#000' : colorTheme}}
      >
        {stars}
      </div>
    );
  }

  render() {
    return this.renderRanking();
  }
}

export default Ranking;
