import React from 'react';
import { storiesOf } from '@storybook/react';

import Ranking from './index';

storiesOf('Ranking', module)
  .add('with ranking 0', () => (<Ranking ranking={0}/>))
  .add('with ranking 2.5', () => (<Ranking ranking={2.5}/>))
  .add('with ranking 5', () => (<Ranking ranking={5}/>))
  .add('with ranking 2.5 + small', () => (<Ranking ranking={2.5} small/>))
  .add('with ranking 2.5 + colorTheme #fd6363', () => (<Ranking ranking={2.5} colorTheme="#fd6363"/>))
  .add('with ranking 2.5 + small + colorTheme #fd6363', () => (<Ranking ranking={2.5} small colorTheme="#fd6363"/>))
;
