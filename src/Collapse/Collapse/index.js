import React from 'react';
import ClassNames from 'classnames';

import './Collapse.scss';

const Collapse = ({
  children,
  isOpen,
}) => {
  return (
    <div
      className={ClassNames(
        'collapse-style',
        {'collapsed': !isOpen},
      )}
    >
      {children}
    </div>
  );
}

export default Collapse;
