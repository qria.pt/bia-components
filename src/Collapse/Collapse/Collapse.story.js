import React from 'react';
import { storiesOf } from '@storybook/react';

import Collapse from './index';

class CollapseStory extends React.PureComponent {
  constructor(props){
    super(props);

    this.state = {
      isOpen: false,
    }
  }

  toggleCollapse = () => {
    const {isOpen} = this.state;
    this.setState({isOpen: !isOpen});
  }

  render() {
    const {isOpen} = this.state;
    return (
      <div onClick={this.toggleCollapse}>
        {'Click me to toggle collapse'}
        <Collapse isOpen={isOpen}>
          <div style={{borderColor: 'blue', borderWidth: '1px', borderStyle: 'solid'}}>
            <h3>You can put any react node as child</h3>
            <p>Like this very informative paragraph</p>
            <ul>
              <li>Or</li>
              <li>This</li>
              <li>Very</li>
              <li>Usefull</li>
              <li>List</li>
            </ul>
          </div>
        </Collapse>
      </div>
    );
  }
}

storiesOf('Collapse', module)
  .add('with isOpen', () => (<CollapseStory />));
