import React from 'react';
import ClassNames from 'classnames';
import {BodyScroll} from '../../index';

import './SideCollapse.scss';

const SideCollapse = ({
  children,
  isOpen,
  noBackground,
}) => {
  isOpen ? BodyScroll.lock() : BodyScroll.unlock();;
  return(
    <React.Fragment>
      {!noBackground &&
        <div className={ClassNames(
          'side-collapse__background',
          'h-100',
          'w-100',
          'position-absolute',
          {'side-collapse__background--faded': !isOpen}
        )}/>
      }
      <div className={ClassNames(
        'side-collapse__body',
        'position-fixed',
        {'side-collapse--closed': !isOpen}
      )}>
        {children}
      </div>
    </React.Fragment>
  );
}

export default SideCollapse;
