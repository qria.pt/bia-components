import React, {PureComponent} from 'react';
import ClassNames from 'classnames';

import Subitems from './Subitems';
import {BodySroll} from '../index';
import HamburgerMenuIcon from '../Assets/Images/hamburger-menu.svg';
import './HamburgerMenu.scss';

class HamburgerMenu extends PureComponent {
  constructor(props) {
    super(props);

    this.state={
      subitems: {subitems: []},
      isOpen: false,
      isSubitems: false,
    };
  }

  openMenu = () => {
    this.setState({
      isOpen: true,
      isSubitems: false,
    });
    BodySroll.lock();
  }

  closeMenu = () => {
    this.setState({
      isOpen: false,
    });
    BodySroll.unlock();
  }

  backMenu =() => {
    this.setState({
      isSubitems: false,
    });
  }

  handleItemClick = (item) => {
    const { handleClick } = this.props;
    if(item.subitems) {
      this.setState({
        subitems: item,
        isSubitems: true,
      });
    } else {
      handleClick(item);
    }
  }

  render() {
    const {
      isOpen,
      subitems,
      isSubitems,
    } = this.state;
    const {
      items,
      handleClick,
    } = this.props;
    return(
      <React.Fragment>
        <img
          className="hamburger-menu mx-2 cursor-pointer"
          src={HamburgerMenuIcon}
          alt="sandwitch menu"
          onClick={this.openMenu}
        />

        <div className={ClassNames(
          'hamburger-menu__background',
          'position-fixed',
          {'hamburger-menu__background--faded': !isOpen}
        )}/>
        <div className={ClassNames(
          'hamburger-menu__body',
          'position-fixed',
          {'hamburger-menu--closed': !isOpen}
        )}>
          <div className="hamburger-menu__header text-uppercase d-flex justify-content-between p-2">
            <div className="m-2">
              {items.title ? items.title : 'Menu'}
            </div>
            <span className="fa fa-times hamburger-menu__close p-2" onClick={this.closeMenu}/>
          </div>
          {items.list.map((item, index) => (
            <div
              key={index}
              className="hamburger-menu__item d-flex justify-content-between align-items-center p-3 cursor-pointer"
              onClick={() => this.handleItemClick(item)}
            >
              {item.title}
              {item.subitems &&
                <span className="far fa-angle-right hamburger-menu__arrow gradient-text"/>
              }
            </div>
          ))}
        </div>

        <Subitems
          item={subitems}
          handleClick={handleClick}
          handleClose={this.closeMenu}
          handleBack={this.backMenu}
          isOpen={isOpen && isSubitems}
        />
      </React.Fragment>
    );
  }
}

export default HamburgerMenu;
