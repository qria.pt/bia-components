import React from 'react';
import { storiesOf } from '@storybook/react';

import HamburgerMenu from './index';

storiesOf('HamburgerMenu', module)
  .add('with items and subitems', () => (
    <div className="d-flex align-items-center pl-3" style={{backgroundColor: 'blue', height: '3.5rem'}}>
      <HamburgerMenu items={items} backText="voltar"/>
    </div>
  ));

const items = {
  title: 'Categorias',
  list: [
    {
      'id': 1,
      'title': 'Tecnologia',
      'subitems': [
        {
          'id': 2,
          'title': 'Computadores & Pcs',
        },
        {
          'id': 3,
          'title': 'Cameras & Fotografia',
        },
        {
          'id': 4,
          'title': 'TV & Audio',
        },
        {
          'id': 5,
          'title': 'Telemóveis & Acessórios',
        },
      ],
    },
    {
      'id': 6,
      'title': 'Desporto & Calçado',
      'subitems': [
        {
          'id': 39,
          'title': 'Sapatilhas',
        },
        {
          'id': 40,
          'title': 'Chuteiras',
        },
        {
          'id': 41,
          'title': 'Calçado Formal',
        },
        {
          'id': 42,
          'title': 'Calçado de Corrida',
        },
      ],

    },
    {
      'id': 7,
      'title': 'Brinquedos & Consolas',
      'subitems': [
        {
          'id': 43,
          'title': 'Computadores & Pcs',
        },
        {
          'id': 44,
          'title': 'Cameras & Fotografia',
        },
        {
          'id': 45,
          'title': 'TV & Audio',
        },
        {
          'id': 46,
          'title': 'Telemóveis & Acessórios',
        },
      ],
    },
    {
      'id': 8,
      'title': 'Beleza & Bem-estar',
    },
    {
      'id': 9,
      'title': 'Joias & Acessórios',
    },
    {
      'id': 10,
      'title': 'Desporto',
    },
    {
      'id': 11,
      'title': 'Entretenimento',
    },
    {
      'id': 12,
      'title': 'Livros',
    },
    {
      'id': 13,
      'title': 'Lar & Decoração',
    },
    {
      'id': 14,
      'title': 'Eletrodomésticos',
    },
    {
      'id': 15,
      'title': 'Sabor Gourmet',
    },
    {
      'id': 16,
      'title': 'Artesanato',
    },
  ],
}
