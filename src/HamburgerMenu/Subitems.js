import React, {PureComponent} from 'react';
import ClassNames from 'classnames';

import './HamburgerMenu.scss';

class Subitems extends PureComponent {
  render() {
    const {
      isOpen,
      item,
      handleClick,
      handleBack,
      handleClose,
    } = this.props;
    return(
      <div className={ClassNames(
        'hamburger-menu__body',
        'position-fixed',
        {'hamburger-menu--closed': !isOpen}
      )}>
        <div className="hamburger-menu__actions d-flex justify-content-between align-items-center p-2">
          <div className="d-flex p-2" onClick={handleBack}>
            <span className="far fa-angle-left hamburger-menu__arrow mr-2"/>
            <div className="hamburger-menu__back text-uppercase">
              Back
            </div>
          </div>
          <span
            className="far fa-times hamburger-menu__close p-2"
            onClick={handleClose}
          />
        </div>

        <div className="hamburger-menu__header hamburger-menu__header--subitem justify-content-center text-uppercase d-flex p-3">
          {item.title}
        </div>

        {item.subitems.map((subitem) => (
          <div
            key={subitem.id}
            className="hamburger-menu__item hamburger-menu__item--subitem d-flex justify-content-between align-items-center p-3"
            onClick={() => handleClick(subitem)}
          >
            {subitem.title}
          </div>
        ))}
      </div>
    );
  }
}

export default Subitems;
