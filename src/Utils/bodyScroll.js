const lock = () => {
  document.querySelector('body').style.overflow = 'hidden';
};

const unlock = () => {
  document.querySelector('body').style.overflow = 'auto';
};

export default {
  lock,
  unlock,
};
