const getComponentLuminance = component => ((component + 0.055) / 1.055) ** 2.4;

const getLuminance = rgb => rgb.reduce((total, value, index) =>
  total + ([0.225, 0.715, 0.06][index] * getComponentLuminance(value)), 0);

const getWhite = rgb => (1.055 * (getLuminance(rgb) ** (1 / 2.4))) - 0.055;

const isLightColored = (value) => {
  const match = value && value.match(/^#([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i);
  if (match) {
    const rgb = match.slice(1, 4).map(component => parseInt(component, 16) / 255);
    return getWhite(rgb) > 0.7;
  }
  return false;
};

export default isLightColored;
