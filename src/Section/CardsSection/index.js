import React from 'react';
import ClassNames from 'classnames';

import './CardsSection.scss';
import {SectionButton} from '../../index';

const CardsSection =({
  title,
  children,
  buttonText,
  buttonAction,
  colorTheme,
  gradient,
  white,
}) => (
  <section className={ClassNames(
    'cards-section',
    'd-flex flex-column position-relative mx-auto w-100 flex-lg-wrap',
    'flex-lg-row justify-content-lg-between align-items-lg-center pt-lg-3',
    {
      'cards-section--gradient': gradient,
      'contained': !gradient,
      'carde-section--white': white,
      'pb-2': !!buttonText && buttonText !== '',
    }
  )}>
    <h2 className={ClassNames(
      'cards-section__title mx-3',
      {
        'cards-section__title--white': white,
        'mt-4 my-lg-4': !white,
      }
    )}>
      {title}
    </h2>
    <ul className="cards-section__list d-flex pt-4 pb-3 pt-lg-2 order-lg-1 w-100">
      {children}
    </ul>
    {!!buttonText && buttonText !== '' &&
      <SectionButton
        text={buttonText}
        colorTheme={colorTheme}
        onClick={buttonAction}
      />
    }
  </section>
);

export default CardsSection;
