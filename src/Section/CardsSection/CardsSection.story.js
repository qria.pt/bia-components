import React from 'react';
import { storiesOf } from '@storybook/react';

import CardsSection from './index';
import { ProductCard } from '../../index';

const CardsSectionStory = ({
  title,
  buttonText,
  gradient,
}) => (
  <CardsSection title={title} buttonText={buttonText} gradient={gradient}>
    {products.map(product => <ProductCard product={product}/>)}
  </CardsSection>
);
storiesOf('CardsSection', module)
  .add('with title', () => <CardsSectionStory title="Cards Section Title" />)
  .add('with buttonText', () => <CardsSectionStory title="Cards Section Title" buttonText="Button Text" />)
  .add('with buttonText + gradient', () => <CardsSectionStory title="Cards Section Title" buttonText="Button Text" gradient />)
;

const products = [
  {
    title: 'Brisket swine dolor uiorisket swine',
    oldPrice: 29.50,
    price: 22.50,
    score: 4,
  },
  {
    title: 'Brisket swine dolor uiorisket swine',
    oldPrice: 29.50,
    price: 22.50,
    score: 4,
  },
  {
    title: 'Brisket swine dolor uiorisket swine',
    oldPrice: 29.50,
    price: 22.50,
    score: 4,
  },
  {
    title: 'Brisket swine dolor uiorisket swine',
    oldPrice: 29.50,
    price: 22.50,
    score: 4,
  },
  {
    title: 'Brisket swine dolor uiorisket swine',
    oldPrice: 29.50,
    price: 22.50,
    score: 4,
  },
];
