import React from 'react';
import { storiesOf } from '@storybook/react';

import ProductsSection from './index';

storiesOf('ProductsSection', module)
  .add('with products + title', () => <ProductsSection products={products} title="Title" />)
  .add('with products + title + buttonText', () => <ProductsSection products={products} title="Title" buttonText="Button Text" />)
  .add('with products + title + buttonText + colorTheme', () => <ProductsSection products={products} title="Title" buttonText="Button Text" colorTheme="#ff0000" />)
  .add('with products + title + buttonText + sale', () => <ProductsSection products={products} title="Title" buttonText="Button Text" sale />)
  .add('with products + title + buttonText + featured', () => <ProductsSection products={products} title="Title" buttonText="Button Text" featured />)
  .add('with products + title + buttonText + sale + colorTheme', () => <ProductsSection products={products} title="Title" buttonText="Button Text" colorTheme="#ff0000" sale />)
;


const products = [
  {
    product: {
      title: 'Brisket swine',
      oldPrice: 29.50,
      price: 22.50,
      images:[],
      score: 4,
      productSlug: 'brisket',
    },
    store: {
      storeSlug: 'store',
      name: 'Jay Karol',
      subtitle: 'Produtos biológicos',
      logo: 'http://www.shopthings.com/image/cache/catalog/Synthetic-Wig-Women-Hairstyle-Afro-Wigs-for-Black-Women-Long-Kinky-Curly-Synthetic-Black-Wig-Natural-32711472548-6973-800x800.jpeg',
    },
  },
  {
    product: {
      title: 'Brisket swine dolor uiorisket swine',
      oldPrice: 29.50,
      price: 22.50,
      images:[],
      score: 4,
      productSlug: 'brisket',
    },
    store: {
      storeSlug: 'store',
      name: 'Jay Karol',
      subtitle: 'Produtos biológicos',
      logo: 'http://www.shopthings.com/image/cache/catalog/Synthetic-Wig-Women-Hairstyle-Afro-Wigs-for-Black-Women-Long-Kinky-Curly-Synthetic-Black-Wig-Natural-32711472548-6973-800x800.jpeg',
    },
  },
  {
    product: {
      title: 'Brisket swine dolor uiorisket swine',
      oldPrice: 29.50,
      price: 22.50,
      images:[],
      score: 4,
      productSlug: 'brisket',
    },
    store: {
      storeSlug: 'store',
      name: 'Jay Karol',
      subtitle: 'Produtos biológicos',
      logo: 'http://www.shopthings.com/image/cache/catalog/Synthetic-Wig-Women-Hairstyle-Afro-Wigs-for-Black-Women-Long-Kinky-Curly-Synthetic-Black-Wig-Natural-32711472548-6973-800x800.jpeg',
    },
  },
  {
    product: {
      title: 'Brisket swine dolor uiorisket swine',
      oldPrice: 29.50,
      price: 22.50,
      images:[],
      score: 4,
      productSlug: 'brisket',
    },
    store: {
      storeSlug: 'store',
      name: 'Jay Karol',
      subtitle: 'Produtos biológicos',
      logo: 'http://www.shopthings.com/image/cache/catalog/Synthetic-Wig-Women-Hairstyle-Afro-Wigs-for-Black-Women-Long-Kinky-Curly-Synthetic-Black-Wig-Natural-32711472548-6973-800x800.jpeg',
    },
  },
  {
    product: {
      title: 'Brisket swine dolor uiorisket swine',
      oldPrice: 29.50,
      price: 22.50,
      images:[],
      score: 4,
      productSlug: 'brisket',
    },
    store: {
      storeSlug: 'store',
      name: 'Jay Karol',
      subtitle: 'Produtos biológicos',
      logo: 'http://www.shopthings.com/image/cache/catalog/Synthetic-Wig-Women-Hairstyle-Afro-Wigs-for-Black-Women-Long-Kinky-Curly-Synthetic-Black-Wig-Natural-32711472548-6973-800x800.jpeg',
    },
  },
  {
    product: {
      title: 'Brisket swine dolor uiorisket swine',
      oldPrice: 29.50,
      price: 22.50,
      images:[],
      score: 4,
      productSlug: 'brisket',
    },
    store: {
      storeSlug: 'store',
      name: 'Jay Karol',
      subtitle: 'Produtos biológicos',
      logo: 'http://www.shopthings.com/image/cache/catalog/Synthetic-Wig-Women-Hairstyle-Afro-Wigs-for-Black-Women-Long-Kinky-Curly-Synthetic-Black-Wig-Natural-32711472548-6973-800x800.jpeg',
    },
  },
];
