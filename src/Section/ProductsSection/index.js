import React, {PureComponent} from 'react';
import ClassNames from 'classnames';
import PropTypes from 'prop-types';

import './ProductsSection.scss';
import {CardsSection, ProductCard} from '../../index';
import Customer from '../../Assets/Images/customer.svg';

const ProductsSection = ({
  products,
  title,
  buttonText,
  buttonAction,
  colorTheme,
  sale,
  featured,
  recent,
}) => {
  return (
    <div className={ClassNames(
      'products-section d-flex flex-column position-relative w-100',
      {
        'products-section--sale pb-3': sale,
        'mt-3': !sale && !recent,
        'mt-0': featured,
      },
    )}>
      {sale &&
        <img src={Customer} className="products-section__sale-icon position-absolute d-lg-none" alt="bia.pt" />
      }
      <CardsSection
        title={title}
        buttonText={buttonText}
        buttonAction={buttonAction}
        colorTheme={colorTheme}
        white={recent}
      >
        {products.map(({product, store}, index) => (
          <li key={index} className={ClassNames(
            'd-inline-flex',
            {'d-lg-none': (index > 3) || (featured && index > 1)},
          )}>
            <ProductCard
              product={product}
              store={store}
              sale={sale}
              featured={featured}
              colorTheme={colorTheme}
            />
          </li>
        ))}
      </CardsSection>
    </div>
  );
};

ProductsSection.propTypes = {
  products: PropTypes.array.isRequired,
  title: PropTypes.string.isRequired,
  buttonText: PropTypes.string,
  buttonAction: PropTypes.func,
  colorTheme: PropTypes.string,
  sale: PropTypes.bool,
  featured: PropTypes.bool,
  recent: PropTypes.bool,
};

ProductsSection.defaultProps = {
  colorTheme: '#6d3dee',
  sale: false,
  featured: false,
};

export default ProductsSection;
