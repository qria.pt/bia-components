import React from 'react';
import ClassNames from 'classnames';

import {CardsSection, ReviewCard} from '../../index';
import './ReviewsSection.scss';

const ReviewsSection =({
  title,
  reviews,
  buttonText,
  buttonAction,
  colorTheme,
}) => (
  <div className="reviews-section">
    <CardsSection
      title={title}
      buttonText={buttonText}
      buttonAction={buttonAction}
      colorTheme={colorTheme}
    >
      {reviews.map((review,index,) => (
        <li key={index} className={ClassNames(
          'd-inline-flex mx-2',
          {'d-lg-none': index > 2}
        )}>
          <ReviewCard review={review}/>
        </li>
      ))}
    </CardsSection>
  </div>
);

export default ReviewsSection;
