import React from 'react';
import { storiesOf } from '@storybook/react';

import ReviewsSection from './index';

storiesOf('ReviewsSection', module)
  .add('with reviews + title', () => <ReviewsSection reviews={reviews} title="Title" />)
  .add('with reviews + title + button text', () => <ReviewsSection reviews={reviews} title="Title" buttonText="ButtonText" />)
  .add('with reviews + title + button text + colorTheme', () => <ReviewsSection reviews={reviews} title="Title" buttonText="ButtonText" colorTheme="#ff0000" />)
;

const reviews = [
  {
    clientName: 'Angelina Germmanota',
    clientProfile: '',
    productTitle: 'Brisket swine dolor uniorisket swine',
    productImage: '',
    comment: 'Peça unica com um preço muito simpático. Veio bem embrulhado e chegou rápido. Gostei!',
    date: '24/04/2019',
    score: 4,
  },
  {
    clientName: 'Angelina',
    clientProfile: '',
    productTitle: 'Brisket swine dolor uniorisket swine',
    productImage: '',
    comment: 'Peça unica com um preço muito simpático. Veio bem embrulhado e chegou rápido. Gostei!',
    date: '24/04/2019',
    score: 4,
  },
  {
    clientName: 'Angelina',
    clientProfile: '',
    productTitle: 'Brisket swine dolor uniorisket swine',
    productImage: '',
    comment: 'Peça unica com um preço muito simpático. Veio bem embrulhado e chegou rápido. Gostei!',
    date: '24/04/2019',
    score: 4,
  },
  {
    clientName: 'Angelina',
    clientProfile: '',
    productTitle: 'Brisket swine dolor uniorisket swine',
    productImage: '',
    comment: 'Peça unica com um preço muito simpático. Veio bem embrulhado e chegou rápido. Gostei!',
    date: '24/04/2019',
    score: 4,
  },
  {
    clientName: 'Angelina',
    clientProfile: '',
    productTitle: 'Brisket swine dolor uniorisket swine',
    productImage: '',
    comment: 'Peça unica com um preço muito simpático. Veio bem embrulhado e chegou rápido. Gostei!',
    date: '24/04/2019',
    score: 4,
  },
];
