import React from 'react';
import { storiesOf } from '@storybook/react';

import BiaButton from './index';

storiesOf('BiaButton', module)
  .add('default with children', () => (
    <BiaButton>
      Bia Button
    </BiaButton>
  ))
  .add('with small', () => (
    <BiaButton small>
      Bia Button
    </BiaButton>
  ))
  .add('with isLoading', () => (
    <BiaButton isLoading>
      Bia Button
    </BiaButton>
  ))
  .add('with dropdown', () => (
    <BiaButton dropdown>
      Bia Button
    </BiaButton>
  ))
  .add('with disabled', () => (
    <BiaButton disabled>
      Bia Button
    </BiaButton>
  ))
  .add('with gradient royalty', () => (
    <BiaButton gradient="royalty">
      Royalty
    </BiaButton>
  ))
  .add('with gradient summer', () => (
    <BiaButton gradient="summer">
      Summer
    </BiaButton>
  ))
  .add('with gradient sunset', () => (
    <BiaButton gradient="sunset">
      Sunset
    </BiaButton>
  ))
  .add('with gradient pink', () => (
    <BiaButton gradient="pink">
      Pink
    </BiaButton>
  ))
  .add('with gradient spring', () => (
    <BiaButton gradient="spring">
      Spring
    </BiaButton>
  ))
  .add('with gradient river', () => (
    <BiaButton gradient="river">
      River
    </BiaButton>
  ))
;

