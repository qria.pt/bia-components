import React, {Component} from 'react';
import ClassNames from 'classnames';

import './BiaButton.scss';

const BiaButton = ({
  type,
  onClick,
  isLoading,
  children,
  disabled,
  gradient,
  dropdown,
  small,
}) => {
  return (
    <button
      onClick={onClick}
      className={ClassNames(
        'bia-button',
        'text-center w-100',
        {
          'cursor-pointer': !disabled,
          'bia-button--small': small,
          'bia-button--disabled': disabled,
          [`bia-button--${gradient}`]: !!gradient,
          'dropdown-toggle': dropdown,
        },
      )}
      type={type}
      disabled={disabled}
    >
      {isLoading
        ? <span key="loading" className="far fa-spin fa-spinner fa-fw" />
        : children
      }
    </button>
  );
}

export default BiaButton;
