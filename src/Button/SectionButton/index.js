import React from 'react';

import { isLightColored } from '../../index';
import './SectionButton.scss';

const SectionButton =({
  text,
  colorTheme,
  onClick,
}) => {
  const color = isLightColored(colorTheme) ? '#000' : colorTheme;
  return (
    <div className="px-2 pr-lg-4">
      <button
        className="section-button text-center cursor-pointer d-lg-flex p-lg-3"
        onClick={onClick}
        style={(colorTheme)
          ? {
            color,
            borderColor: color,
          }
          : null
        }
      >
        {text}
      </button>
    </div>
  );
};

export default SectionButton;
