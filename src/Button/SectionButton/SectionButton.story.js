import React from 'react';
import { storiesOf } from '@storybook/react';

import SectionButton from './index';

storiesOf('SectionButton', module)
  .add('with text', () => <SectionButton text="Button Text" />)
  .add('with text + colorTheme', () => <SectionButton text="Button Text" colorTheme="#ff0000" />)
;
