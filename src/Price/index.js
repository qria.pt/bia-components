import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './Price.scss';

class PriceComponent extends PureComponent {
  static propTypes = {
    price: PropTypes.string.isRequired,
    oldPrice: PropTypes.string,
    periodicity: PropTypes.string,
  };

  render() {
    const {
      price,
      oldPrice,
      periodicity,
    } = this.props;

    return (
      <div className='price__wrapper'>
        <div className={
          classnames('price__label price__label--current', {
            'price__label--highlight': oldPrice,
          })}
        >
          {price}
          {periodicity &&
            <span className='price__label-periodicity'>
              {periodicity}
            </span>
          }
        </div>
        {oldPrice ?
          <div className='price__label price__label--old'>
            {oldPrice}
          </div>
          :
          <div />
        }
      </div>
    );
  }
}

export default PriceComponent;
