import React from 'react';
import { storiesOf } from '@storybook/react';

import Price from './index';

storiesOf('Price', module)
  .add('with amount', () => <Price price="€ 100,00" />)

  .add('with old price', () => <Price price="€ 100,00" oldPrice="€ 110,00" />)

  .add('with subscription', () => <Price price="€ 100,00" periodicity="mês" />)

  .add('with subscription + old price', () =>
    <Price price="€ 100,00" oldPrice="€ 110,00" periodicity="mês" />)
;
