import React, {useRef} from 'react';

import './search.scss';

const SearchBox = ({
  placeholder,
  handleSearch,
}) => {
  const searchboxRef = useRef(null);
  return (
    <div className="search__container d-flex align-items-center">
      <input
        ref={searchboxRef}
        className="search__box"
        autoComplete="on"
        type="text"
        name="search"
        placeholder={placeholder}
        onKeyUp={(e) => {
          if (e.keyCode === 13) {
            handleSearch(e.target.value)
          }
        }}
      />
      <button onClick={() => handleSearch(searchboxRef.current.value)}>
        <span className="search__icon far fa-search" />
      </button>
    </div>
  );
}

export default SearchBox;
