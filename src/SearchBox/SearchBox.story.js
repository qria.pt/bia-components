import React from 'react';
import { storiesOf } from '@storybook/react';

import SearchBox from './index';

storiesOf('SearchBox', module)
  .add('with items and subitems', () => (
    <div className="p-3" style={{backgroundColor: 'blue'}}>
      <SearchBox placeholder="Search" handleSearch={(term) => console.log(`You searched ${term}`)}/>
    </div>
  ));
