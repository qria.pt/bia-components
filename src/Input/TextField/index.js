import React from 'react';
import ClassNames from 'classnames';
import { Field, ErrorMessage } from 'formik';

import '../Input.scss';

const TextField = ({
  name,
  type,
  handleBlur,
  disabled,
  placeholder,
  readonly,
  noErrorMessage,
  ...props
}) => {
  return (
    <div className="position-relative">
      <Field
        {...props}
        name={name}
        render={({
          field: {
            onBlur,
            ...field,
          },
          form: {
            errors: {[name]: error},
            touched: {[name]: touched},
          },
        }) => (
          <input
            {...field}
            readOnly={readonly}
            type={type ? type : 'text'}
            disabled={disabled}
            placeholder={placeholder}
            onBlur={ (params) =>{
              onBlur(params);
              if(!!handleBlur) {
                handleBlur();
              }
            }}
            className={ClassNames(
              'input w-100 mb-3',
              {'input--invalid': (touched && !!error)},
            )}
          />
        )}
      />
      { !noErrorMessage &&
        <ErrorMessage
          name={name}
          render={msg => [
            <span key="error-icon" className="far fa-exclamation-circle input__error-icon position-absolute"/>,
            <div key="error-arrow" className="input__error-arrow position-absolute"/>,
            <div key="error-message" className="input__error-message py-1 px-2 d-flex align-items-center justify-content-center position-absolute">{msg}</div>,
          ]}
        />
      }
    </div>
  );
}

export default TextField;
