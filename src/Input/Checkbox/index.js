import React, {Fragment} from 'react';
import { Field, ErrorMessage } from 'formik';
import ClassNames from 'classnames';

import './Checkbox.scss';

const Checkbox = ({
  name,
  disabled,
  children,
  className,
  ...props,
}) => (
  <div className="d-flex mb-3 position-relative">
    <Field
      {...props}
      name={name}
      render={({
        field,
        form: {
          errors: {[name]: error},
          touched: {[name]: touched},
        },
      }) => (
        <Fragment>
          <input
            {...field}
            type="checkbox"
            disabled={disabled}
            className={ClassNames('checkbox', {'checkbox--invalid': (touched && !!error)} )}
          />
          <label
            htmlFor={name}
            className={ClassNames(
              'my-0 mr-0',
              'checkbox__label',
              {
                [className]: !!className,
                'checkbox__label--disabled': disabled,
              }
            )}
          >
            {children}
          </label>
        </Fragment>
      )}
    />
    <ErrorMessage
      name={name}
      render={msg => [
        <div key="error-arrow" className="checkbox__error-arrow position-absolute"/>,
        <div key="error-message" className="checkbox__error-message py-1 px-2 d-flex align-items-center justify-content-center position-absolute">{msg}</div>,
      ]}
    />
  </div>
);

export default Checkbox;
