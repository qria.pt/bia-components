import React from 'react';
import ClassNames from 'classnames';
import { FieldArray, ErrorMessage, Field } from 'formik';

import Plus from '../../Assets/Images/plus.svg';
import Minus from '../../Assets/Images/minus.svg';

import './ArrayField.scss';
import '../Input.scss';

const ArrayField = ({
  name,
  placeholder,
  fields,
  disabled,
  label,
  small,
  ...props
}) => {
  return (
    <div className="position-relative">
      <FieldArray
        {...props}
        name={name}
        render={({
          form: {
            errors: {[name]: error},
            touched: {[name]: touched},
          },
          ...arrayHelpers,
        }) => {
          return (
            <div className="w-100">
              <div className="d-flex aling-items-center mb-2">
                <label className={ClassNames(
                  'array-field__label',
                  'mb-0',
                  {
                    'array-field__label--disabled': disabled,
                    'array-field__label--invalid': (touched && !!error),
                  }
                )}>
                  {label}
                </label>
                <button
                  className="array-field__button mx-2"
                  disabled={disabled}
                  onClick={() => arrayHelpers.push('')}
                  type="button"
                >
                  <img className="array-field__button-icon" src={Plus} alt="adicionar campo para imagem" />
                </button>
              </div>
              <ul className={ClassNames(
                'array-field__list',
                {'d-flex flex-wrap': small},
              )}>
                {fields.map((field, index) => (
                  <div className="array-field position-relative pr-2 pl-1" key={index}>
                    <Field
                      name={`${name}.${index}`}
                      disabled={disabled}
                      placeholder={placeholder.concat(' ', index + 1)}
                      className={ClassNames(
                        'input w-100 mb-3',
                        {'input--invalid': (touched && !!error)},
                      )}
                    />
                    <button
                      className="array-field__button flex-center position-absolute"
                      disabled={disabled}
                      onClick={() => arrayHelpers.remove(index)}
                      type="button"
                    >
                      <img src={Minus} alt={`remover imagem ${index + 1}`} className="array-field__button-icon" />
                    </button>
                  </div>
                ))}
              </ul>
            </div>
          );
        }
      }/>
      <ErrorMessage
        name={name}
        render={ msg => [
          <div key="error-arrow" className="array-field__error-arrow position-absolute"/>,
          <div key="error-message" className="array-field__error-message py-1 px-2 d-flex align-items-center justify-content-center position-absolute">{msg}</div>,
        ]}
      />
    </div>
  );
}

export default ArrayField;
