import React from 'react';
import ClassNames from 'classnames';
import { Field, ErrorMessage } from 'formik';

import '../Input.scss';

const TextArea = ({
  name,
  handleBlur,
  disabled,
  placeholder,
  ...props
}) => {
  return (
    <div className="position-relative">
      <Field
        {...props}
        name={name}
        type="text"
        render={({
          field: {
            onBlur,
            ...field,
          },
          form: {
            errors: {[name]: error},
            touched: {[name]: touched},
          },
        }) => (
          <textarea
            {...field}
            disabled={disabled}
            placeholder={placeholder}
            onBlur={ (params) =>{
              onBlur(params);
              if(!!handleBlur) {
                handleBlur();
              }
            }}
            className={ClassNames(
              'input input--textarea',
              'w-100 mb-3 d-block',
              {'input--invalid': (touched && !!error)},
            )}
          />
        )}
      />
      <ErrorMessage
        name={name}
        render={ msg => [
          <div key="error-arrow" className="input__error-arrow position-absolute"/>,
          <div key="error-message" className="input__error-message py-1 px-2 d-flex align-items-center justify-content-center position-absolute">{msg}</div>,
        ]} />
    </div>
  );
}

export default TextArea;
