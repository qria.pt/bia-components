import React, {useState} from 'react';
import ClassNames from 'classnames';
import { Field, ErrorMessage } from 'formik';

import '../Input.scss';

const PasswordField = ({
  name,
  disabled,
  placeholder,
  ...props,
}) => {
  const [passwordVisible, setPasswordVisible] = useState(false);

  return (
    <div className="position-relative">
      <Field
        {...props}
        name={name}
        render={({
          field,
          form: {
            errors: {[name]: error},
            touched: {[name]: touched},
          },
        }) => (
          <div className="position-relative">
            <input
              {...field}
              type={passwordVisible ? 'text' : 'password'}
              disabled={disabled}
              placeholder={placeholder}
              className={ClassNames(
                'input',
                'w-100 py-3 mb-3',
                {'input--invalid': (touched && !!error),},
              )}
            />
            <span className={ClassNames(
              'far',
              'input__password-button',
              'position-absolute',
              'p-3',
              {
                'fa-eye-slash': !passwordVisible,
                'fa-eye': passwordVisible,
                'input__password-button--visible': passwordVisible,
                'input__password-button--error': (touched && !!error),
              }
            )} onClick={() => setPasswordVisible(!passwordVisible)}/>
          </div>
        )}
      />

      <ErrorMessage
        name={name}
        render={msg => [
          <div key="error-arrow" className="input__error-arrow position-absolute"/>,
          <div key="error-message" className="input__error-message py-1 px-2 d-flex align-items-center justify-content-center position-absolute">{msg}</div>,
        ]} />
    </div>
  );
}

export default PasswordField;
