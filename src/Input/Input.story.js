import React from 'react';
import { storiesOf } from '@storybook/react';
import {Formik, Form} from 'formik';

import TextField from './TextField';
import PasswordField from './PasswordField';
import TextArea from './TextArea';
import ArrayField from './ArrayField';
import Checkbox from './Checkbox';

const validate = () => 'This field is invalid!';

storiesOf('Input', module)
  .add('TextField', () => (
    <Formik
      initialValues={{
        input: '',
        small: '',
        disabled: '',
        invalid: '',
      }}

      render={({touched: {invalid: invalidTouched}, setFieldTouched}) => {
        if(!invalidTouched){
          setFieldTouched('invalid', true, true);
        }
        return (
          <Form>
            <TextField
              name="input"
              placeholder="Text Field"
            />
            <TextField
              name="disabled"
              disabled
              placeholder="Disabled"
            />
            <TextField
              name="invalid"
              validate={validate}
              placeholder="Invalid"
            />
          </Form>
        )
      }}
    >
    </Formik>
  ))
  .add('PasswordField', () => (
    <Formik
      initialValues={{
        input: '',
        small: '',
        disabled: '',
        invalid: '',
      }}

      render={({touched: {invalid: invalidTouched}, setFieldTouched}) => {
        if(!invalidTouched){
          setFieldTouched('invalid', true, true);
        }
        return (
          <Form>
            <PasswordField
              name="input"
              placeholder="Password Field"
            />
            <PasswordField
              name="disabled"
              disabled
              placeholder="Disabled"
            />
            <PasswordField
              name="invalid"
              validate={validate}
              placeholder="Invalid"
            />
          </Form>
        )
      }}
    >
    </Formik>
  ))
  .add('TextArea', () => (
    <Formik
      initialValues={{
        input: '',
        disabled: '',
        invalid: '',
      }}

      render={({touched: {invalid: invalidTouched}, setFieldTouched}) => {
        if(!invalidTouched){
          setFieldTouched('invalid', true, true);
        }
        return (
          <Form>
            <TextArea
              name="input"
              placeholder="Text Area"
            />
            <TextArea
              name="disabled"
              disabled
              placeholder="Disabled"
            />
            <TextArea
              name="invalid"
              validate={validate}
              placeholder="Invalid"
            />
          </Form>
        )
      }}
    >
    </Formik>
  ))
  .add('ArrayField', () => (
    <Formik
      initialValues={{
        small: ['Initial Value', ''],
        input: [''],
        disabled: [''],
        invalid: [''],
      }}

      validate={() => ({
        invalid: validate(),
        [`invalid.0`]: validate(),
      })}

      render={({
        values: {input, small, disabled, invalid},
        touched: {invalid: invalidTouched},
        setFieldTouched
      }) => {
        if(!invalidTouched){
          setFieldTouched('invalid', true, true);
        }
        return (
          <Form>
            <ArrayField
              name="input"
              placeholder="Array Field"
              label="Array Field"
              fields={input}
            />
            <ArrayField
              name="small"
              placeholder="Array Field"
              label="Array Field Small"
              small
              fields={small}
            />
            <ArrayField
              name="disabled"
              disabled
              label="Disabled"
              placeholder="Disabled"
              fields={disabled}
            />
            <ArrayField
              name="invalid"
              label="Invalid"
              placeholder="Invalid"
              fields={invalid}
              validate={validate}
            />
          </Form>
        )
      }}
    >
    </Formik>
  ))
  .add('Chekbox', () => (
    <Formik
      initialValues={{
        input: false,
        disabled: true,
        invalid: false,
      }}

      render={({touched: {invalid: invalidTouched}, setFieldTouched}) => {
        if(!invalidTouched){
          setFieldTouched('invalid', true, true);
        }
        return (
          <Form>
            <Checkbox
              name="input"
            >
              Checkbox
            </Checkbox>
            <Checkbox
              name="disabled"
              disabled
            >
              Disabled
            </Checkbox>
            <Checkbox
              name="invalid"
              validate={validate}
            >
              Invalid
            </Checkbox>
          </Form>
        )
      }}
    >
    </Formik>
  ))
;
