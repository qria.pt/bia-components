import React from 'react';
import { storiesOf } from '@storybook/react';

import ProductCard from './index';

storiesOf('ProductCard', module)
  .add('with product without like', () => <ProductCard onClick={onClick} product={{...product, liked: undefined}} />)
  .add('with product not liked', () => <ProductCard onClick={onClick} product={product} />)
  .add('with product liked', () => <ProductCard onClick={onClick} product={{...product, liked: true}} />)
  .add('with sale', () => <ProductCard onClick={onClick} product={product} sale />)
  .add('with featured', () => <ProductCard onClick={onClick} product={product} featured />)
  .add('with sale + featured', () => <ProductCard onClick={onClick} product={product} sale featured />)
  .add('with colorTheme', () => <ProductCard onClick={onClick} product={product} colorTheme="#fd6363" />)
  .add('with light colorTheme', () => <ProductCard onClick={onClick} product={product} colorTheme="#ffffff" />)
;

const onClick = () => {};

const product = {
  title: 'Brisket swine dolor uiorisket swine',
  oldPrice: 29.50,
  price: 22.50,
  liked: false,
  score: 4,
};
