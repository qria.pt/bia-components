import React from 'react';
import PropTypes from 'prop-types';
import ClassNames from 'classnames';

import { Ranking, isLightColored } from '../../index';
import Bag from '../../Assets/Images/bag';
import './ProductCard.scss';

const formatPrice = (price) => `${Number.parseFloat(price).toFixed(2)}€`;
const formatDiscount = (price, oldPrice) => `-${100 - Number.parseFloat(price / oldPrice * 100).toFixed(0)}%`;

const ProductCard =({
  product,
  sale,
  featured,
  colorTheme,
  onClick,
  onAddToCartClick,
  onLikeClick,
  horizontal,
}) => {
  const contrastColor = isLightColored(colorTheme) ? '#000' : '#fff';
  return (
    <div className={ClassNames(
      'product-card position-relative mx-2',
      {
        'pt-lg-3': !featured,
        'product-card--featured mx-lg-0 p-lg-2 p-xl-3': featured,
        'product-card--horizontal': horizontal,
      },
    )}>
      {product.liked !== undefined &&  !horizontal &&
        <button
          onClick={onLikeClick}
          className={ClassNames(
            'product-card__icon flex-center position-absolute cursor-pointer',
            {'d-lg-none': featured},
          )}
          style={{
            color: product.liked ? colorTheme : contrastColor,
            backgroundColor: product.liked ? contrastColor : colorTheme,
          }}
        >
          <span className={`fas fa-${product.liked ? 'check' : 'heart'}`}/>
        </button>
      }
      <div
        onClick={onClick}
        className={ClassNames(
          'product-card__content d-inline-flex',
          {
            'cursor-pointer': !!onClick,
            'flex-column': !horizontal,
            'flex-lg-wrap': featured,
          },
        )}
      >
        <div className={ClassNames(
          'flex-center mr-lg-3 product-card__image',
          {
            'ml-lg-3': !featured,
            'mr-xl-4': featured,
          },
        )}>
          {(product.images && product.images[0])
            ? (
              <img
                src={`${process.env.REACT_APP_AWS_BASE_URL}${product.images[0]}`}
                className="product-card__image"
                alt={product.title}
              />
            ) : (
              <span className="far fa-file-image" alt={product.title}/>
            )
          }
        </div>
        <div className={ClassNames(
          'product-card__body d-flex flex-column justify-content-between flex-grow-1',
          {'d-lg-block': featured}
        )}>
          <h3 className={ClassNames(
            'product-card__title m-2 mt-lg-3',
            {
              'm-lg-3': !featured,
              'ml-lg-0 mb-lg-2': featured,
            },
          )}>
            {product.title}
          </h3>

          {horizontal &&
            <div className="product-card__store mx-2 mx-lg-3 d-lg-none">
              {store.name}
            </div>
          }

          <div className={ClassNames(
            'm-2 pb-1 mb-lg-0 pb-lg-0',
            'd-flex justify-content-between align-items-center',
            'flex-lg-column align-items-lg-start',
            {
              'mx-lg-3': !featured,
              'mx-lg-0': featured,
            },
          )}>
            <Ranking ranking={product.score} small={!featured} />

            <span className="product-card__price d-flex flex-column justify-content-between align-items-center flex-lg-row">
              {!!product.oldPrice &&
                <span className={ClassNames(
                  'product-card__price--old my-lg-0 mr-lg-2',
                  {'d-none d-lg-inline': !sale}
                )}>
                  {formatDiscount(product.price, product.oldPrice)}
                </span>
              }
              {formatPrice(product.price)}
            </span>
          </div>
        </div>
      </div>
      <button
        onClick={onAddToCartClick}
        className={ClassNames(
          'product-card__button d-none d-lg-flex justify-content-center align-items-center',
          {'position-absolute': featured},
        )}
        style={ featured ? {backgroundColor: colorTheme} : null}
      >
        <Bag className="product-card__button-icon"/>
        Add ao carrinho
      </button>
    </div>
  );
};

ProductCard.propTypes = {
  product: PropTypes.object.isRequired,
  sale: PropTypes.bool,
  featured: PropTypes.bool,
  colorTheme: PropTypes.string,
  handleClick: PropTypes.func,
  horizontal: PropTypes.bool,
};

ProductCard.defaultProps = {
  sale: false,
  featured: false,
  colorTheme: '#6d3dee',
  handleClick: () => {},
  horizontal: false,
};

export default ProductCard;
