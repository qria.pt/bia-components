import React from 'react';

import './ReviewCard.scss';
import {Ranking} from '../../index';

const ReviewCard = ({
  review: {
    clientName,
    clientProfile,
    productTitle,
    productImage,
    comment,
    date,
    score,
  },
}) => {
  return (
    <div className="review-card d-inline-block p-2">
      <div className="d-lg-flex flex-row-reverse justify-content-end">
        <div className="review-card__header d-flex flex-column flex-wrap align-content-start m-2 mx-lg-3 pb-lg-1 flex-lg-row">
          {(!!clientProfile && clientProfile !== '')
            ? (
              <img
                src={`${process.env.REACT_APP_AWS_BASE_URL}${clientProfile}`}
                className="review-card__profile mx-2 mx-lg-0"
                alt={clientName}
              />
            ) : (
              <div className="review-card__profile flex-center mx-2 mx-lg-0">
                <span className="far fa-file-image" alt={clientName}/>
              </div>
            )
          }

          <div className="review-card__name-container d-flex flex-column mb-lg-3">
            <span className="review-card__name">
              {clientName.split(' ')[0]}
            </span>
            <span className="review-card__date d-none d-lg-inline">
              {date}
            </span>
          </div>
          <Ranking ranking={score} small />
        </div>

        {(!!productImage && productImage !== '')
          ? (
            <img
              src={`${process.env.REACT_APP_AWS_BASE_URL}${productTmage}`}
              className="review-card__product my-2 my-lg-0"
              alt={productTitle}
            />
          ) : (
            <div className="review-card__product flex-center my-2 my-lg-0">
              <span className="far fa-file-image" alt={productTitle}/>
            </div>
          )
        }
      </div>

      <p className="review-card__comment">
        {comment}
      </p>
    </div>
  );
}

export default ReviewCard;
