import React from 'react';
import { storiesOf } from '@storybook/react';

import ReviewCard from './index';

storiesOf('ReviewCard', module)
  .add('review', () => <ReviewCard review={review} />);

const review = {
  clientName: 'Angelina',
  clientProfile: '',
  productTitle: 'Brisket swine dolor uniorisket swine',
  productImage: '',
  comment: 'Peça unica com um preço muito simpático. Veio bem embrulhado e chegou rápido. Gostei!',
  date: '24/04/2019',
  score: 4,
};
