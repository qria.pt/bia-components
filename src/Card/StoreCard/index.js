import React from 'react';

import { Ranking } from '../../index';

import './StoreCard.scss';

const StoreCard =({
  store,
  newProducts,
}) => (
  <div
    style={{backgroundColor: store.colorTheme}}
    className="
      store-card
      d-inline-flex flex-column align-items-center
      d-lg-flex flex-lg-row
      mx-1 px-lg-3 pt-lg-3 pb-lg-2 mx-lg-2
    "
  >
    <div className="d-flex flex-column align-items-center flex-grow-1">
      {!!store.logo
        ? (
          <img
            src={`${process.env.REACT_APP_AWS_BASE_URL}${store.logo}`}
            className="store-card__logo"
            alt={store.name}
          />
        ) : (
          <div className="store-card__logo flex-center">
            <span className="far fa-file-image" alt={store.name}/>
          </div>
        )
      }
      <div className="store-card__name">
        {store.name}
      </div>
      <div className="store-card__subtitle mt-2">
        {store.subtitle}
      </div>
      <div className="d-none d-lg-flex mt-3">
        <Ranking ranking={store.score} small />
        <span className="store-card__reviews ml-3">{store.reviews} reviews</span>
      </div>
    </div>
    <div className="store-card__extra d-none d-lg-flex flex-wrap justify-content-end">
      {store.products.slice(0, 4).map((product, index) => (product.images && product.images[0])
        ? (
          <img
            src={`${process.env.REACT_APP_AWS_BASE_URL}${product.images[0]}`}
            className="store-card__product"
            alt={product.title}
          />
        ) : (
          <div className="store-card__product flex-center">
            <span className="far fa-file-image" alt={product.title}/>
          </div>
        )
      )}
      <span className="store-card__new-products w-100 text-center py-2">
        Esta loja tem {newProducts} novos Protudos
      </span>
    </div>
  </div>
);

export default StoreCard;
