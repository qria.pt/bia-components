import React from 'react';
import { storiesOf } from '@storybook/react';

import StoreCard from './index';

storiesOf('StoreCard', module)
  .add('with Store and newProducts', () => <StoreCard store={store} newProducts={20}  />)
;

const store = {
  name: 'Jay Karol',
  subtitle: 'Produtos biológicos',
  score: 4.5,
  reviews: 17,
  products:[
    {
      images: [],
      title: 'produto 1'
    },
    {
      images: [],
      title: 'produto 2'
    },
    {
      images: [],
      title: 'produto 3'
    },
    {
      images: [],
      title: 'produto 4'
    },
  ]
};
